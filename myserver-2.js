const express = require('express');
const app = express();
const port = 8000;

const Vigenere = require('caesar-salad').Vigenere;

app.get('/encode/:name', (req,res) => {
    res.send(Vigenere.Cipher('vik').crypt(req.params.name));
  });
app.get('/decode/:name', (req,res) => {
    res.send(Vigenere.Decipher('vik').crypt(req.params.name));
  });


app.listen(port, () => {
    console.log('We are live on ' + port);
  });





 
// Caesar.Cipher('c').crypt('abc-0123456789@example.com');
// //=> cde-0123456789@gzcorng.eqo
 
// Caesar.Decipher('c').crypt('cde-0123456789@gzcorng.eqo');
// //=> abc-0123456789@example.com

// Xkvcnkm